package pim.microservices.catalog.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import pim.microservices.catalog.entity.Produk;

public interface ProdukDao extends PagingAndSortingRepository<Produk, String>{
	
}
