package pim.microservices.catalog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import pim.microservices.catalog.dao.ProdukDao;
import pim.microservices.catalog.entity.Produk;

@RestController
//anotasi untuk controller

public class ProductController {
	@Autowired private ProdukDao produkDao;
	//anotasi untuk
	
	@GetMapping("/produk/")
	//anotasi untuk
	public Page<Produk> dataProduk(Pageable page){
		return produkDao.findAll(page);
	}
	
}
